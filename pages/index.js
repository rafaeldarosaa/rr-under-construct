import Head from "next/head";
import Container from "../components/container";
import Intro from "../components/intro";
import Layout from "../components/layout";
import Header from "../components/header";
import OctadeskScript from "../components/scripts/octadesk-script.js";

export default function Index() {
  return (
    <>
      <Layout>
        <Head>
          <title>Rafael Rosa - Web Developer</title>
        </Head>
        <Container>
          <Header />
          <Intro />
        </Container>
        <OctadeskScript />
      </Layout>
    </>
  );
}
