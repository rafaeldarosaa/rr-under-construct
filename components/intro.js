export default function Intro() {
  return (
    <section className="flex flex-col lg:flex-row items-start justify-around mt-0 lg:mt-8 mb-8 md:mb-24 md:mt-6 w-full h-full">
      <div className="flex flex-col items-center justify-center">
        <h1 className="text-xl md:text-5xl tracking-tight leading-loose md:pr-2 max-w-5xl text-white font-light text-center">
          {/* Você ou seu negócio, precisa de um website? */}
          Site em desenvolvimento
        </h1>
        <h2 className="text-sm text-center lg:text- md:text-xl font-normal tracking-tight leading-normal md:pr-2 max-w-2xl text-white font-light">
          Entre em contato através das opções a seguir:
        </h2>
        <div className="flex flex-row items-center justify-between mt-8">
          <a
            href="https://www.linkedin.com/in/rafael-rosa-a31b05109/"
            title="Rafael Rosa LinkedIn"
            target="_blank"
          >
            <div className="btn flex item-center leading-tight mr-8 px-6 py-3">
              LinkedIn
            </div>
          </a>

          <a
            href="https://wa.me/5555991782222"
            title="Rafael Rosa Whatsapp"
            target="_blank"
          >
            <div className="btn flex px-3 py-2 item-center leading-tight px-6 py-3">
              WhatsApp
            </div>
          </a>
        </div>
      </div>

      <div className="mt-20 hidden lg:block rr-shadow">
        <img src="images/rafaelrosa-profile.png" width="372" height="372" />
      </div>

      <div className="mt-10 block lg:hidden flex justify-end mx-auto rr-shadow">
        <img src="images/rafaelrosa-profile.png" width="200" height="200" />
      </div>
    </section>
  );
}