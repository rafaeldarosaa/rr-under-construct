export default function Header() {
  return (
    <nav className="flex items-center justify-start py-6 lg:py-12 w-full">
      <div className="flex items-center flex-shrink-0 text-accent-2 mr-6">
        <img
          src="/images/rafaelrosa.svg"
          width="120"
          height="47"
          alt="Rafael Rosa Desenvolvimento Web"
          className="rounded mr-2"
        />
      </div>
    </nav>
  );
}
