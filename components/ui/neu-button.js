import React from "react";
import styles from "./neu-button.module.css";

const NeuButton = React.forwardRef((props, ref) => (
  <button ref={ref} className={styles.neubutton + " btn px-3 py-2"}>
    {props.btnText}
  </button>
));

export default NeuButton;
