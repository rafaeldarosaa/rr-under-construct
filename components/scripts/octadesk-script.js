export default function OctadeskScript() {
  return (
    <script
      type="text/javascript"
      async
      dangerouslySetInnerHTML={{
        __html: `(function (o, c, t, a, d, e, s, k) {
          o.octadesk = o.octadesk || {};
          o.octadesk.chatOptions = {
            subDomain: a,
            showButton: d,
            openOnMessage: e
          };
          s = c.getElementsByTagName("body")[0];
          k = c.createElement("script");
          k.async = 1;
          k.src = t;
          s.appendChild(k);
        })(window, document, 'https://cdn.octadesk.com/embed.js', 'o57573-05b31',  true, true);
`,
      }}
    />
  );
}
