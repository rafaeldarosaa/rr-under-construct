export default function Layout({ children }) {
  return (
    <>
      <div className="h-full lg:min-h-screen bg-bg">
        <main>{children}</main>
      </div>
    </>
  );
}
